import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDB {
	
	
	static Connection connection = null;
	static String databaseName = "";
	static String url = "jdbc:mysql://localhost:3306/" + databaseName + "?characterEncoding=latin1";
	
	static String username = "root";
	static String password = "password";

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		try (Connection conn = DriverManager.getConnection(url, username, password);
				Statement stmt = conn.createStatement();
			)
		{
			String sql = "INSERT INTO foodapp.DH_ORDER VALUES (22, 2, 5, 2);";
			stmt.executeUpdate(sql);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		try (Connection conn1 = DriverManager.getConnection(url, username, password);
				Statement stmt = conn1.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM foodapp.DH_ORDER");
			) {
			while (rs.next()) {
				System.out.print("ID: " + rs.getInt("OrderID"));
				System.out.print("COST: " + rs.getInt("Cost"));
				System.out.print("RESTAURANT: " + rs.getInt("Restaurant_ID"));
				System.out.println("PID: " + rs.getInt("PID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
